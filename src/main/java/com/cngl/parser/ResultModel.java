package com.cngl.parser;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ResultModel {

    private String domain;
    private List<String> ip;
    private List<String> ipAdresses;
    private String ipAdress2;
    private String submask;

    public ResultModel(){
        ip=new ArrayList<>();
        ipAdresses=new ArrayList<>();
    }
}