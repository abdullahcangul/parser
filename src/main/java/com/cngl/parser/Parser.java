package com.cngl.parser;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.validator.routines.InetAddressValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

//Stringi al
//parçala
//Test et(Apache commons)-Tür belirle
//Ayrıntılı parçalama islemini yap
//hatalı giris varsa exception fırlat



public class Parser {

    //          1           2           3           4
    //    "rrgoogle.com,10.0.0.5,[192.168.1.6-50],10.0.0.1/24"

    ResultModel result=new ResultModel();

    public ResultModel parse(String text) throws Exception {
        //Temel Parcalama islemi virgüle göre parçalar
        List<String> parts= smash(text);

        // Type  Belirler ve gereken parcalama işlemi yapar
        findType(parts);

        return result;
    }

    private void findType(List<String> parts) throws Exception {
       for(String part:parts){
          check(part);
        }
    }
    //Type belirler hatalı girişte hata fırlatır
    private void check(String part) throws Exception {
        Pattern patternDomain = Pattern.compile("^(?!:\\/\\/)([a-zA-Z0-9-_]+\\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\\.[a-zA-Z]{2,11}?$");

        InetAddressValidator validator = InetAddressValidator.getInstance();

        //Domain ayırıcı
        if (patternDomain.matcher(part).matches()){
            result.setDomain(part);
        }
        //ip ayırıcı
        else if(validator.isValid(part)){
            result.getIp().add(part);
            result.setIp(result.getIp());
        }
        else{
            //ip listesi döner
            if (part.contains("-")){
                result.setIpAdresses(smash3(part));
            }
            //ip ve submask döner
            else if(part.contains("/")){
               List<String> list= smash4(part);
                result.setIpAdress2(list.get(0));
                result.setSubmask(list.get(1));
            }
            //Hatalı giriş yapılmış
            else{
                throw new Exception("ERROR:INCORRECT ENTRY");
            }
        }
    }

    //[192.168.1.2-100]-> alır ip adreslerini liste halinde döner
    private List<String> smash3(String s) throws Exception {

        InetAddressValidator validator = InetAddressValidator.getInstance();

        s=s.replace("[","");
        s=s.replace("]","");
        //192.168.1.2-100
        String[] parts=s.split("-");
        //100
        Integer max=Integer.parseInt(parts[1]);
        //192,168,1,2
        String[] partsForMin=parts[0].split("[.]");
        //2
        Integer min=Integer.parseInt(partsForMin[partsForMin.length-1]);
        //192.168.1
        String firstPart=parts[0].substring( 0,parts[0].lastIndexOf(".")+1);
        //en küçükden en büyüge listeye alma
        List<String> list=new ArrayList<String>();
        for (int i=min;i<=max;i++){
            if (!validator.isValid(firstPart+i))
                throw new Exception("ERROR:INCORRECT ENTRY");
            list.add(firstPart+i);
        }
        return list;
    }

    //10.0.0.1/24-> alır submask ve ip adreslerini  liste halinde döner.
    private List<String> smash4(String s) throws Exception {

        SubnetUtils  subnet = new SubnetUtils(s);

        List<String> list=new ArrayList<String>();

        list.add(subnet.getInfo().getAddress());

        list.add(subnet.getInfo().getNetmask());

        return list;
    }

    //Girilen Stringi virgüle(,) göre  parçalara ayırır ,boşluk siler
    public List<String> smash(String text){
        List<String> list=Arrays.asList( text.split(","));
        list=list.stream().map(x->x.trim())
                .collect(Collectors.toList());
        return list;
    }
}